﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VowelCounterLibrary;
using VowelCounterProgram.Resources;

namespace VowelCounterProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            VowelCounterHelper vowelCounterHelper = new VowelCounterHelper();
            Console.WriteLine(AppResources.WelcomeMessage);
            Console.WriteLine(AppResources.EnterMessageRequest);
            var answer = Console.ReadLine();
            Console.WriteLine(AppResources.Answer.Replace("{0}", vowelCounterHelper.Count(answer).ToString()));
            Console.WriteLine(AppResources.ExitMessage);
            Console.Read();

        }
    }
}
