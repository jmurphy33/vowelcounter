﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VowelCounterLibrary.Resources;

namespace VowelCounterLibrary
{
    public class VowelCounterHelper
    {
        public int Count(string word)
        {
            var vowels = Vowels.VowelsToUse.Split(',');
            int count = 0;

            foreach (var item in word.ToCharArray())
            {
                if (vowels.Contains(item.ToString()))
                {
                    count++;
                }
            }

            return count;
        }
    }
}
