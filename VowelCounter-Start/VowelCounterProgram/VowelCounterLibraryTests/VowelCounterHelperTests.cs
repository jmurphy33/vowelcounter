﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using VowelCounterLibrary;

namespace VowelCounterLibraryTests
{
    [TestClass]
    public class VowelCounterHelperTests
    {
        VowelCounterHelper _vowelCounterHelper;

        [TestMethod]
        public void VowelCount_CountsTheVowelsInAGivenWord_ReturnsCorrectNumber()
        {
            // Arrange
            _vowelCounterHelper = new VowelCounterHelper();
            int vowelCount = 3;
            string word = "aaa";

            // Act
            var result =_vowelCounterHelper.Count(word);

            // Assert
            Assert.AreEqual(vowelCount, result);
        }

        [TestMethod]
        public void VowelCount_ZeroVowels_ReturnsCorrectNumber()
        {
            // Arrange
            _vowelCounterHelper = new VowelCounterHelper();
            int vowelCount = 0;
            string word = "bbqqtt";

            // Act
            var result = _vowelCounterHelper.Count(word);

            // Assert
            Assert.AreEqual(vowelCount, result);
        }

        [TestMethod]
        public void VowelCount_CapsAndNonCaps_ReturnsCorrectNumber()
        {
            // Arrange
            _vowelCounterHelper = new VowelCounterHelper();
            int vowelCount = 10;
            string word = "AaEeIiOoUu";

            // Act
            var result = _vowelCounterHelper.Count(word);

            // Assert
            Assert.AreEqual(vowelCount, result);
        }
    }
}
